import re
from flask import Flask, render_template, request, render_template_string

app = Flask(__name__)

s = {
    "therizino-1": {
        "url": "https://musnaz.org/wp-content/uploads/2021/03/therizino-e1616463582517.jpg",
        "title": "A fossil of Therizinosaurus. Courtesy of Museum of Norhtern Arizona."
    },
    "brachio-2": {
        "url": "https://www.museumfuernaturkunde.berlin/sites/default/files/sauriersaal_04_c_carola-radke-mfn.jpg",
        "title": "A fossil of Brachiosaurus. Courtesy of Museum of Museum für Naturkunde."
    },
    "allosaur-3": {
        "url": "https://images.ctfassets.net/x2t7lek2vf7h/1X1dBG9wH7uLRHynQs9dXa/69a04bd637cfcec8495691e2a7b94d48/SMM_Allosaurus-2__1_.jpg?w=3900&h=2925&fl=progressive&q=50&fm=jpg",
        "title": "A fossil of Allosaurus. Courtesy of Science Museum of Minnesota."
    },
    "trilobite-1": {
        "url": "https://oumnh.ox.ac.uk/sites/default/files/oumnh/images/media/img_4567.jpg",
        "title": "A fossil of Trilobite. Courtesy of Oxford University Museum of Natural History."
    },
    "parasaur-2": {
        "url": "https://museum.arnabontempsmuseum.com/where_can_you_find_the_parasaurolophus.PNG",
        "title": "A fossil of Parasaurolophus. Courtesy of The American Museum of Natural History."
    },
    "smilodon": {
        "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Smilodon_Smithsonian_National_Museum_of_Natural_History.jpg/800px-Smilodon_Smithsonian_National_Museum_of_Natural_History.jpg",
        "title": "A fossil of smilodon. Courtesy of Smithsonian National Museum of Natural History."
    }
}

def check(string):
    blacklist = ["__init__", "__globals__", "nl", "subprocess", "config", "\\{\\{", "\\}\\}", "\\[", "\\]", " ", "update", "request"]
    for word in blacklist:
        if re.search(word, string):
            return False
    return True

@app.route("/", methods=["POST", "GET"])
def home():
    cont = {}
    if request.method == "POST":
        if not "search" in request.form or not request.form["search"]:
            cont["status"] = "no_query"
            cont["images"] = s
            return render_template("index.html", context=cont)
            
        query = request.form["search"]
        
        if len(query) > 65:
            cont["status"] = "over_limit"
            return render_template("index.html", context=cont)
        
        if not check(query):
            cont["status"] = "red_alert"
            return render_template("index.html", context=cont)

        for i in s:
            if re.search(f"{query}", s[i]["title"], flags=re.IGNORECASE):
                if not "images" in cont:
                    cont["images"] = {}
                cont["images"][i] = s[i]
                
        if not cont:
            cont["status"] = "not_found"
        else:
            cont["status"] = "found"
            cont["found"] = len(cont["images"])
            
        cont["query"] = query
        ret = render_template("index.html", context=cont)
        return render_template_string(ret)
        
    cont["status"] = "get"
    cont["images"] = s
    return render_template("index.html", context=cont)
    
if __name__ == "__main__":
    app.run("0.0.0.0", port=1337)
