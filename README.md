# Prehistoric Gallery

original author: sl0ck (Vacation Galery - COMPFEST 14)

---

## Flag

```
c0nGr4TS_jUAra_2_wRECkiT_k4k_aJarIN_saY4_pLS_2b3a0b8947
```

## Description
My friend has collected various photos of dinosaur fossil around the world. Check out his collection!

```
http://<ip>:<port>
```

**Note**: For this challenge, there is a special prize of points (half of the final challenge points) for writeups that contain the shortest payload that is shorter than the limit. If several payloads have the same (shortest) length, all will get the prize.

## Difficulty
medium

## Hints
* how does one filter in jinja2 again?

## Tags
Web, SSTI, filtered SSTI

## Deployment
- Make sure you already install docker >= 19.03.13 and docker-compose >= 1.27.4.
- Command to run container:
    ```
    docker-compose up -d --build
    ```
- Command to stop container:
    ```
    docker-compose down
    ```

## Notes
> Intentionally left empty